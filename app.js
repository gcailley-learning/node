const path = require("path");

const express = require("express");
const mongoConnect = require("./util/database");

const errorController = require("./controllers/error");

const app = express();

app.set("view engine", "ejs");
app.set("views", "views");

const adminRoutes = require("./routes/admin");
const shopRoutes = require("./routes/shop");
const User = require("./models/user");

app.use(express.json());
app.use(express.urlencoded());
app.use(express.static("public"));

app.use((req, res, next) => {
  User.findById("6054c78bce46f272c7e5761a")
    .then((user) => {
      req.user = new User(user.name, user.email, user.cart, user._id);
      next();
    })
    .catch((err) => {
      console.error(err);
      next();
    });
});

app.use("/admin", adminRoutes);
app.use(shopRoutes);

app.use(errorController.get404);

mongoConnect.mongoConnect((client) => {
  app.listen(8080);
//   const io = require('socket.io')(server);
//   io.on('connection', socket => {
//     console.log('client connected');
//   });

  console.log("app running : 8080");
  console.log("");
});
