
const rootDir = require("../util/path");
const path = require("path");
const fs = require("fs");

class ProductService {
  static instance;
  static productsPath = path.join(rootDir, "data", "products.json");

  _products = [];
  constructor() {}
  static getInstance() {
    if (!ProductService.instance) {
      ProductService.instance = new ProductService();
    }
    return ProductService.instance;
  }

  loadFromFile() {
    fs.readFile(ProductService.productsPath, (err, fileContent) => {
      let products = [];
      if (!err) {
        products = JSON.parse(fileContent);
        this._products = [...products];
      }
    });
  }

  fetchAll(cb) {
    this.loadFromFile();
    cb(this._products);
  }

  save(product) {
    this._products.push(product);
    fs.writeFile(ProductService.productsPath, JSON.stringify(this._products), (err) => {
      console.error(err);
    });
  }
}

module.exports = {
  service: ProductService.getInstance(),
};
