class UserService {
  static instance;
  _users = [];
  constructor() {}
  static getInstance() {
    if (!UserService.instance) {
      UserService.instance = new UserService();
      UserService.instance.add('user A');
      UserService.instance.add('user B');
      UserService.instance.add('user ');
    }
    return UserService.instance;
  }
  fetchAll() {
    return [...this._users];
  }
  add(username) {
    this._users.push(username);
  }
}

module.exports = { 
    service: UserService.getInstance()
};
