const deleteProduct = (btn, idProduct) => {
  const productElt = btn.closest("article");
  fetch(`/admin/product/${idProduct}`, {
    method: "DELETE",
  })
    .then((response) => {
      return response.json();
    })
    .then((json) => {
      productElt.parentNode.removeChild(productElt);
    })
    .catch((err) => {
      console.error(err);
    });
};
