const fs = require("fs");
const UserService = require("./services/user.service");

const requestHandler = (req, res) => {
  if (req.url === "/") {
    res.setHeader("Content-Type", "text/html");
    res.write("<html>");
    res.write("<head><title>My First Page</title></head>");
    res.write(
      '<body><form action="/message" method="POST"><input type="text" name="message"><button type="submit">Send</button></form></body>'
    );

    res.write("</html>");
    return res.end();
  }

  if (req.url === "/message" && req.method === "POST") {
    console.log("message");

    const body = [];
    req.on("data", (chunk) => {
      console.log("data");
      console.log(chunk);
      body.push(chunk);
    });

    return req.on("end", () => {
      const parsedBody = Buffer.concat(body).toString();
      const [messsage, data] = parsedBody.split("=");
      return fs.writeFile("message.txt", data, (err) => {
        res.statusCode = 302;
        res.setHeader("Location", "/");
        return res.end();
      });
    });
  }

  if (req.url === "/users" && req.method === "GET") {
    const users = UserService.service.fetchAll();
    res.setHeader("Content-Type", "text/html");
    res.write("<html>");
    res.write("<head><title>Users</title></head>");
    res.write("<body>");
    users.forEach((element) => {
      res.write(`${element} <br>`);
    });

    res.write(
      '<form action="/new-user" method="POST"><input type="text" name="username"><button type="submit">Send</button></form></body>'
    );
    res.write("<body>");
    res.write("</html>");


    return res.end();
  }

  if (req.url === "/new-user" && req.method === "POST") {
    const body = [];
    req.on("data", (chunk) => {
      body.push(chunk);
    });

    return req.on("end", () => {
      const parsedBody = Buffer.concat(body).toString();
      const [key, username] = parsedBody.split("=");
      UserService.service.add(`User : ${username}`);
      res.statusCode = 302;
      res.setHeader("Location", "/users");
      return res.end();
    });
  }

  res.setHeader("Content-Type", "text/html");
  res.write("<html>");
  res.write("<head><title>My First Page</title></head>");
  res.write("<body>My First Page</body>");
  res.write("</html>");
  res.end();
};

// module.exports = requestHandler;

module.exports = {
  handler: requestHandler,
};
